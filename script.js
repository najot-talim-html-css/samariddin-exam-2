$(".owl-carousel-1").owlCarousel({
    loop: true,
    nav: true,
    dots: true,
    margin: 20,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      1000: {
        items: 2,
      },
    },
  });AOS.init({
    duration: 3000,
    once: true,
  });const loading = document.getElementById("loading");
  setTimeout(() => {
    loading.classList.add("loading-none");
  }, 3000);